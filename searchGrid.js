var fs = require('fs');
var puzzleFile = process.argv[2];

// Horizontal and vertical search helper
var horizontalVerticalSearch = function(CheatSheet, rowChars, rowInt, cols, isReverse, isHorizontal, solutionsObject){
  for (var i = 0; i < CheatSheet.length; i ++){
    let strToCompare = CheatSheet[i].replace(/\s+/g, '');
    for (var j = isReverse ? rowChars.length - 1 : 0; isReverse ? j >= strToCompare.length - 1 : j < cols - strToCompare.length; isReverse ? j -- : j ++) {
      let subString = isReverse ? wordReversal(rowChars.substring(j, j + strToCompare.length)) : rowChars.substring(j, j + strToCompare.length)
      if (subString.match(strToCompare)) {
        if (isHorizontal === true) {
          let cordinate1 = rowInt + ':' + j
          let cordinate2 = rowInt + ':' + (isReverse ? j + subString.length - 1 : j + subString.length - 1)
          let finalCordinate = isReverse ? (cordinate2 + ' ' + cordinate1) : (cordinate1 + ' ' + cordinate2)
          solutionsObject[CheatSheet[i]] = [finalCordinate];
        } else {
          let cordinate1 = rowInt + ':' + j
          let cordinate2 = rowInt + ':' + (isReverse ? j + subString.length - 1 : j + subString.length - 1)
          let finalCordinate = isReverse ? (cordinate2 + ' ' + cordinate1) : (cordinate1 + ' ' + cordinate2)
          solutionsObject[CheatSheet[i]] = [finalCordinate];
        }
      }
    }
  }
};

function wordReversal(wrd) {
  return wrd.split( '' ).reverse( ).join( '' );
}

// Diagonal helper (2 way diag)
var swingRightLeftSearch = function(CheatSheet, rowChars, rowInt, cols, isReverse, diagonal, isdiagRight, solutionsObject){
  for (var i = 0; i < CheatSheet.length; i ++){
    let strToCompare = CheatSheet[i].replace(/\s+/g, '');
    if (strToCompare.length <= cols) {
      for (var j = isReverse ? rowChars.length - 1 : 0; isReverse ? j >= strToCompare.length - 1 : j < cols - strToCompare.length; isReverse ? j -- : j ++) {
        let subString = isReverse ? wordReversal(rowChars.substring(j, j + strToCompare.length)) : rowChars.substring(j, j + strToCompare.length)
        if (subString.match(strToCompare)) {
          if (isdiagRight === true) {
            let cordinate1 = diagonal ? ((cols - 1 - j) + ':' + (cols - rowChars.length + j)) : ((rowInt - j) + ':' + j)
            let cordinate2 = diagonal ? ((cols - j - subString.length) + ':' + (cols - rowChars.length - 1 + j + subString.length)) : ((rowChars.length - j - subString.length) + ':' + (isReverse ? j + subString.length - 1 : j + subString.length - 1))
            let finalCordinate = isReverse ? (cordinate2 + ' ' + cordinate1) : (cordinate1 + ' ' + cordinate2)
            solutionsObject[CheatSheet[i]] = [finalCordinate];
          } else {
            let cordinate1 = diagonal ? ((cols - 1 - j) + ':' + (rowChars.length - 1 - j)) : ((rowInt - j) + ':' + (cols - 1 - j))
            let cordinate2 = diagonal ? ((cols - j - subString.length) + ':' + (rowChars.length - j - subString.length)) : ((rowInt - subString.length - 1) + ':' + (isReverse ? cols - subString.length - 2 : cols - j - subString.length))
            let finalCordinate = isReverse ? (cordinate2 + ' ' + cordinate1) : (cordinate1 + ' ' + cordinate2)
            solutionsObject[CheatSheet[i]] = [finalCordinate];
          }
        }
      }
    }
  }
}

// Determine grid size
var retRowCount = function(line1) {
  var index = 0;
  for(; index < line1.length; index++) {
    if (line1[index] === 'x' || line1[index] === 'X') {
      return parseInt(line1.substring(0, index))
    }
  }
}

var splitOnNewLineToArray = function(stringToCheck){
  var returnArray = [];
  if (stringToCheck.search('\r\n') > 0){
    returnArray = stringToCheck.split('\r\n');
  } else {
    returnArray = stringToCheck.split('\n');
  }
  return returnArray;
};

var generateReverseArray = function(arrayToReverse){
  var returnArray = [];
  for (var i = 0; i < arrayToReverse.length; i ++){
    returnArray[i] = arrayToReverse[i].split('').reverse().join('');
  }
  return returnArray;
};

var generateDiagonalArray = function(arrayToDiagonalize){
  var numberOfRows = arrayToDiagonalize.length;
  var numberOfColumns = arrayToDiagonalize[0].length;
  var numberOfDiagonalRows = numberOfRows + numberOfColumns - 1;
  var returnArray = [];
  for (var i = 0; i < numberOfDiagonalRows; i ++){
    returnArray.push([]);
  }
  for (var k = 0; k < numberOfRows; k ++){
      for (var j = 0, i = k; j <= k; j ++, i--){
          returnArray[k].push(arrayToDiagonalize[i][j]);
      }
  }
  for (var l = numberOfRows; l < numberOfDiagonalRows; l ++){
      for (var m = numberOfRows - 1, n = l - numberOfRows + 1; n < numberOfColumns; m --, n ++){
          returnArray[l].push(arrayToDiagonalize[m][n]);
      }
  }
  for (var i = 0; i < returnArray.length; i ++){
    returnArray[i] = returnArray[i].join('');
  }
  return returnArray;
}

var searchGrid = function(puzzleFile){
  var solutions = {};

  var puzzleCharString = fs.readFileSync(puzzleFile, 'utf8').toString().replace(/,/g, "");
  if (!puzzleCharString){
    console.log('Empty playing board');
    return
  }
  var SplitToArray = splitOnNewLineToArray(puzzleCharString);
  let chartSize = retRowCount(SplitToArray[0]);
  var puzzleCharRowArray = SplitToArray.slice(1, chartSize + 1);
  var SpyList = SplitToArray.slice(chartSize + 1, SplitToArray.length);
  var numberOfPuzzleRows = puzzleCharRowArray.length;
  var numberOfPuzzleColumns = puzzleCharRowArray[0].length;
  var longestDiagonal = numberOfPuzzleRows > numberOfPuzzleColumns ? numberOfPuzzleColumns : numberOfPuzzleRows;

  // FORWARDS & REVERSE HORIZONTAL
  for (var z = 0; z < numberOfPuzzleRows; z ++){
    horizontalVerticalSearch(SpyList, puzzleCharRowArray[z], z, numberOfPuzzleColumns, false, true, solutions)
    horizontalVerticalSearch(SpyList, puzzleCharRowArray[z], z, numberOfPuzzleColumns, true, true, solutions)
  }

  // FORWARDS & REVERSE VERTICAL
  var downArray = [];
  for (var i = 0; i < numberOfPuzzleColumns; i ++){
    downArray.push([]);
  }
  for (var j = 0; j < numberOfPuzzleRows; j ++){
    for (var k = 0; k < puzzleCharRowArray[j].length; k ++){
      downArray[k][j] = puzzleCharRowArray[j][k];
    }
  }
  for (var i = 0; i < downArray.length; i ++){
    downArray[i] = downArray[i].join('');
  }
  numberOfPuzzleRows = downArray.length;
  let downCols = downArray[0].length;
  for (var y = 0; y < numberOfPuzzleRows; y ++){
    horizontalVerticalSearch(SpyList, downArray[y], y, downCols, false, false, solutions)
    horizontalVerticalSearch(SpyList, downArray[y], y, downCols, true, false, solutions)
  }

  // SWING TO THE RIGHT
  var swingRightList = generateDiagonalArray(puzzleCharRowArray);
  numberOfPuzzleRows = swingRightList.length;
  var diagReached = false
  for (var x = 0; x < numberOfPuzzleRows; x ++){
    if (diagReached === false) {
      diagReached = swingRightList[x].length >= longestDiagonal ? true : false;
    }
    swingRightLeftSearch(SpyList, swingRightList[x], x, numberOfPuzzleColumns, false, true, diagReached, solutions)
    swingRightLeftSearch(SpyList, swingRightList[x], x, numberOfPuzzleColumns, true, true, diagReached, solutions)
  }

  // SWING TO THE LEFT
  var ReverseFirstList = generateReverseArray(puzzleCharRowArray);
  var uswingLeftList = generateDiagonalArray(ReverseFirstList);
  numberOfPuzzleRows = uswingLeftList.length;
  var diagonalReached = false
  for (var w = 0; w < numberOfPuzzleRows; w ++){
    if (diagonalReached === false) {
      diagonalReached = uswingLeftList[w].length >= longestDiagonal ? true : false;
    }
    swingRightLeftSearch(SpyList, uswingLeftList[w], w, numberOfPuzzleColumns, false, false, diagonalReached, solutions)
    swingRightLeftSearch(SpyList, uswingLeftList[w], w, numberOfPuzzleColumns, true, false, diagonalReached, solutions)
  }
  var extResult = '';
  if (Object.keys(solutions).length){
    for(u in SpyList) {
      if (SpyList[u] in solutions) {
        extResult += (SpyList[u] + ' ' + solutions[SpyList[u]] + '\r\n');
      }
    }
  } else {
    extResult += 'Sorry -> No input word hints were found'
  }
  console.log(extResult)
  return

};

searchGrid(puzzleFile);